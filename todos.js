var express = require('express');
var mysql = require('mysql');

var router = express.Router();

var pool = mysql.createPool({
    host: 'localhost',
    database: 'testdb',
    user: 'alex_r',
    password: 'alex_r'
});

router.get('/', function (req, res) {
    //load data from DB here
    var todoItems = [];

    pool.getConnection(function (err, connection) {
        connection.query('select * from posts', function (err, rows, fields) {
            if (err) throw err;

            rows.forEach(element => {
                todoItems.push({ id: element.id, desc: element.description });
            });

            res.render('index', {
                title: 'My App',
                items: todoItems
            });
        });
    });
});

router.post('/add', function (req, res) {
    var post = {};
    post.description = req.body.newItem;

    pool.getConnection(function (err, connection) {
        connection.query('insert into posts set ?', post, function (err, rows, fields) {
            if (err) throw err;

            res.redirect('/');
        });
    });
});

module.exports = router;