var express = require('express');
var path = require('path');
var bodyParser = require('body-parser')

var app = express();

//configure app

app.set('view engine','ejs');
app.set('views',path.join(__dirname,'views'));

//use middleware

app.use(bodyParser());
app.use(express.static(path.join(__dirname,'bower_components')));

//define routes

app.use(require('./todos'));

//start server

var port = process.env.PORT || 1337;
app.listen(port, function(){
    console.log('ready on port ' + port);
});









































// const http = require('http');

// const hostname = '127.0.0.1';
// const port = 3000;

// const server = http.createServer((req, res) => {
//   res.statusCode = 200;
//   res.setHeader('Content-Type', 'text/plain');
//   res.end('Hello World\n');
// });

// server.listen(port, hostname, () => {
//   console.log(`Server running at http://${hostname}:${port}/`);
// });
